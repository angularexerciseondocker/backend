# Angular Exercise SpringBoot Backend

* Currently, in development

## Project description

* Backend for the FE https://gitlab.com/angularexerciseondocker/frontend
* Uploaded file data is saved to MySQL database

## Pre-requisites to run the code:
* Maven
* Java JDK 21

## Installation instruction:

* Install Java JDK 21
* Install Maven
* Install MySql database, create a root user
* Optional: install Intellij IDEA Ultimate
* git clone https://gitlab.com/angularexerciseondocker/backend
* rename `/src/main/resources/application.properties.example` to `/src/main/resources/application.properties` and change MySQL username, password to your database username/password:
  * spring.datasource.username=<YOUR_USERNAME>
  * spring.datasource.password=<YOUR_PASSWORD>
* run the project, it will be accessible on http://localhost:8081
* database and table is auto-generated, see all properties in `/src/main/resources/application.properties.example`
* download and run frontend, see https://gitlab.com/angularexerciseondocker/frontend

