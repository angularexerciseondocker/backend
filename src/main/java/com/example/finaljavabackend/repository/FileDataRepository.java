package com.example.finaljavabackend.repository;

import com.example.finaljavabackend.entity.FileData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FileDataRepository extends JpaRepository<FileData, Long>{

}
