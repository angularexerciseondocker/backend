package com.example.finaljavabackend.controller;

import com.example.finaljavabackend.entity.FileData;
import com.example.finaljavabackend.entity.FileDataDto;
import com.example.finaljavabackend.service.FileDataService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Transactional
@RestController
@AllArgsConstructor
@RequestMapping("/api/filedata")
@CrossOrigin("http://localhost:4200")
public class FileDataController {
    FileDataService<FileData> fileDataService;

    @GetMapping({"/", ""})
    public List<FileData> getFileData(){
        return fileDataService.getFilesData();
    }

    @GetMapping("/{id}")
    public FileDataDto getAFileData(@PathVariable("id") Long id){
        return fileDataService.getAFileData(id);
    }

    @PostMapping({"/", ""})
    public FileDataDto addFileData(@RequestBody FileDataDto fileDataDto){
        return fileDataService.addFileData(fileDataDto);
    }

    @PostMapping("/records")
    public List<FileDataDto> addFileDataRecords (@RequestBody List<FileDataDto> fileDataDtos){
        return fileDataService.addFileDataRecords(fileDataDtos);
    }

    @PutMapping
    public FileDataDto updateFileData(@RequestBody FileDataDto generalDto){
        return fileDataService.updateFileData(generalDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<FileData> deleteFileData(@PathVariable("id") Long id){
        fileDataService.deleteFileData(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
