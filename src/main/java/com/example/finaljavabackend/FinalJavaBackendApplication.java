package com.example.finaljavabackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalJavaBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinalJavaBackendApplication.class, args);
    }

}
