package com.example.finaljavabackend.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FileDataDto {
    private Long fileDataId;
    private String dataElement;
    private String description;
    private String dataDictionaryTerm;
    private String businessDictionaryTerm1;
    private String businessDictionaryTerm2;
}
