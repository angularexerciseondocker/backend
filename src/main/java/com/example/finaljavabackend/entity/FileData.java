package com.example.finaljavabackend.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;

@Entity
@Getter
@Setter
@ToString(exclude = "id")
@NoArgsConstructor
@AllArgsConstructor
public class FileData{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String dataElement;
    private String description;
    private String dataDictionaryTerm;
    private String businessDictionaryTerm1;
    private String businessDictionaryTerm2;

    public FileData(String dataElement, String description, String dataDictionaryTerm, String businessDictionaryTerm1, String businessDictionaryTerm2) {
        this.dataElement = dataElement;
        this.description = description;
        this.dataDictionaryTerm = dataDictionaryTerm;
        this.businessDictionaryTerm1 = businessDictionaryTerm1;
        this.businessDictionaryTerm2 = businessDictionaryTerm2;
    }
}
