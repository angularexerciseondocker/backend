package com.example.finaljavabackend.mapper;

import com.example.finaljavabackend.entity.*;
import org.mapstruct.*;


@Mapper(componentModel = "spring")
public interface FileDataMapper {
    @Mapping(target = "id", ignore=true)
    @Mapping(target = "dataElement", source = "dataElement")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "dataDictionaryTerm", source = "dataDictionaryTerm")
    @Mapping(target = "businessDictionaryTerm1", source = "businessDictionaryTerm1")
    @Mapping(target = "businessDictionaryTerm2", source = "businessDictionaryTerm2")
    FileData convertToEntity(FileDataDto fileDataDto);


    @Mapping(target = "fileDataId", ignore=true)
    @Mapping(target = "dataElement", source = "dataElement")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "dataDictionaryTerm", source = "dataDictionaryTerm")
    @Mapping(target = "businessDictionaryTerm1", source = "businessDictionaryTerm1")
    @Mapping(target = "businessDictionaryTerm2", source = "businessDictionaryTerm2")
    FileDataDto convertToFileDataDto(FileData fileData);

    @Mapping(target = "id", expression = "java(fileDataDto.getFileDataId())")
    @Mapping(target = "dataElement", source = "dataElement")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "dataDictionaryTerm", source = "dataDictionaryTerm")
    @Mapping(target = "businessDictionaryTerm1", source = "businessDictionaryTerm1")
    @Mapping(target = "businessDictionaryTerm2", source = "businessDictionaryTerm2")
    void updateEntity(@MappingTarget FileData fileData, FileDataDto fileDataDto);
}
