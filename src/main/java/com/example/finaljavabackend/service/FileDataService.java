package com.example.finaljavabackend.service;

import com.example.finaljavabackend.entity.FileDataDto;
import java.util.List;


public interface FileDataService<FileData> {
    List<FileData> getFilesData();
    FileDataDto getAFileData(Long id);

    FileDataDto addFileData(FileDataDto generalDto);

    List<FileDataDto> addFileDataRecords(List<FileDataDto> fileDataDtos);

    FileDataDto updateFileData(FileDataDto generalDto);
    void deleteFileData(Long id);

}
