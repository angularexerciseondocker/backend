package com.example.finaljavabackend.service;

import com.example.finaljavabackend.entity.*;
import com.example.finaljavabackend.mapper.FileDataMapper;
import com.example.finaljavabackend.repository.FileDataRepository;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@AllArgsConstructor
public class FileDataServiceImpl implements FileDataService<FileData>{
    private FileDataRepository fileDataRepository;
    private final FileDataMapper fileDataMapper;

    @Override
    public List<FileData> getFilesData() {
        return fileDataRepository.findAll();
    }

    @Override
    public FileDataDto getAFileData(Long id) {
        FileData fileData = findById(id);
        //return fileDataMapper.convertToFileDataDto(new FileDataDto(), fileData);
        return fileDataMapper.convertToFileDataDto(fileData);
    }

    @Override
    public FileDataDto addFileData(FileDataDto fileDataDto) {
        FileData fileData = fileDataMapper.convertToEntity(fileDataDto);
        FileData saved = saveFileData(fileData);
        return fileDataMapper.convertToFileDataDto(saved);
    }

    public List<FileDataDto> addFileDataRecords(List<FileDataDto> fileDataDtos){
        FileData fileData;
        List<FileData> fileDataRecords = fileDataDtos.stream()
                .map(fileDataMapper::convertToEntity)
                .toList();
        List<FileData> savedRecords = fileDataRecords.stream()
                .map(this::saveFileData)
                .toList();
        return savedRecords.stream()
                .map(fileDataMapper::convertToFileDataDto)
                .toList();
    }

    @Transactional
    @Override
    public FileDataDto updateFileData(FileDataDto fileDataDto) {
        FileData fileData = findById(fileDataDto.getFileDataId());
        fileDataMapper.updateEntity(fileData, fileDataDto);
        FileData updated = saveFileData(fileData);
        return fileDataMapper.convertToFileDataDto(updated);
    }

    @Override
    public void deleteFileData(Long id) {
        if(fileDataRepository.existsById(id)){
            fileDataRepository.deleteById(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Data not found in repository");
        }
    }

    private FileData saveFileData(FileData fileData){
        return fileDataRepository.saveAndFlush(fileData);
    }

    public FileData findById(Long fileDataId) {
        return fileDataRepository.findById(fileDataId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "FileData not found by id: " + fileDataId));
    }
}
